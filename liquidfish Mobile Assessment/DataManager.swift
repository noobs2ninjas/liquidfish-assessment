//
//  DataManager.swift
//  liquidfish Mobile Assessment
//
//  Created by Nathan Kellert on 10/13/19.
//  Copyright © 2019 Nathan Kellert. All rights reserved.
//

import Foundation
import CoreData
import Alamofire

class DataManager  {
    
    let baseURL = "https://picsum.photos/v2/list?limit=100"
    
    static let sharedInstance = DataManager()

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "DataModel")
        let context = container.viewContext
        context.stalenessInterval = TimeInterval(truncating: false)
        context.mergePolicy = NSMergePolicy(merge: .overwriteMergePolicyType)
        context.automaticallyMergesChangesFromParent = true

        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })

        return container
    }()

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func getDataForPage(_ page: Int?, withCompletion completion: @escaping (_ data: [String: AnyObject]?, _ error: AFError?) -> Void) {
        var params = [
            "limit" : 100
        ]
        
        if page != nil {
            params["page"] = page!
        }
        
        AF.request(baseURL, parameters: params)
            .validate(statusCode: 200..<300)
            .responseJSON { responseJSON in
                switch responseJSON.result {
                case .success(let json):
                    if let data = json as? [String: AnyObject] {
                        completion(data, nil)
                    } else {
                    completion(nil, nil)
                    }
                case .failure(let error):
                    completion(nil, error)
            }
        }
        
    }
    
    
}
