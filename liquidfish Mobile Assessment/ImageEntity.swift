//
//  ImageModel.swift
//  liquidfish Mobile Assessment
//
//  Created by Nathan Kellert on 10/13/19.
//  Copyright © 2019 Nathan Kellert. All rights reserved.
//

import Foundation
import Bolts

enum ImageType {
    case thumbnail, original
}

extension ImageEntity {
    
    typealias DownloadTaskCompletion = ((Data?, Error?) -> Void)
    private func asyncDownload(withCompletion completion: @escaping DownloadTaskCompletion) {
        
        URLSession.shared.dataTask(with: download_url) { (data, response, error) in
            if error != nil {
                print("ERROR: ", error!.localizedDescription)
                completion(nil, error)
            } else if data != nil {
                if let image = UIImage(data: data!) {
                    let newImage = self.resizeImage(image: image, targetSize: CGSize(width: 900, height: 900))
                    let newData = newImage.jpegData(compressionQuality: 1)
                    completion(newData, nil)
                } else {
                    completion(nil, nil)
                }

            } else {
                completion(nil, AsyncDownloadError.noDataRecieved)
            }
        }.resume()
    }
}


