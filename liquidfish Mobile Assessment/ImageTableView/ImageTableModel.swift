//
//  ImageTableModel.swift
//  liquidfish Mobile Assessment
//
//  Created by Nathan Kellert on 10/13/19.
//  Copyright © 2019 Nathan Kellert. All rights reserved.
//

import Foundation
import CoreData

class ImageTableModel {
    
    var page: Int!
    
    init(page: Int) {
        self.page = page
    }
    
    func getFetchRequest() -> NSFetchRequest<NSFetchRequestResult>? {
        let fetchRequest:NSFetchRequest<ImageEntity> = ImageEntity.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "SELF.page ==  %@", page)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "index", ascending: true)]

        return fetchRequest as? NSFetchRequest<NSFetchRequestResult>
    }
}
