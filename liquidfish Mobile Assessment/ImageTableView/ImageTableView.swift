//
//  ImageTableView.swift
//  liquidfish Mobile Assessment
//
//  Created by Nathan Kellert on 10/13/19.
//  Copyright © 2019 Nathan Kellert. All rights reserved.
//

import UIKit

class ImageTableView: UITableView {
    
    var model: ImageTableModel!
    
    init(frame: CGRect, style: Style, page: Int) {
        super.init(frame: frame, style: style)
        
        model = ImageTableModel(page: page)
    }
    
    func setPage(_ page: Int) {
        self.model.page = page
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
