//
//  ImageCell.swift
//  liquidfish Mobile Assessment
//
//  Created by Nathan Kellert on 10/13/19.
//  Copyright © 2019 Nathan Kellert. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {
    
    var imageEntity: ImageEntity!
    
    func setData(_ imageEntity: ImageEntity) {
        self.imageEntity = imageEntity
    }
    
}


