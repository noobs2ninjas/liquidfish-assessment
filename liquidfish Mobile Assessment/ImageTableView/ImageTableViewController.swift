//
//  ImageTableViewController.swift
//  liquidfish Mobile Assessment
//
//  Created by Nathan Kellert on 10/13/19.
//  Copyright © 2019 Nathan Kellert. All rights reserved.
//

import UIKit

class ImageTableViewController: UITableViewController {
    
    var page:Int! {
        didSet {
            if imageTableView != nil {
                imageTableView!.setPage(page)
            }
        }
    }
    
    var imageTableView: ImageTableView? {
        get {
            return self.tableView as? ImageTableView ?? nil
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            self.tableView = ImageTableView(frame: self.view.frame, style: .insetGrouped, page: page)
        } else {
            self.tableView = ImageTableView(frame: self.view.frame, style: .grouped, page: page)
        }
    }

}
