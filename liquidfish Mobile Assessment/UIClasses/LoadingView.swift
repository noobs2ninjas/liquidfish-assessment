//
//  LoadingView.swift
//  NewChatApp
//
//  Created by Nathan Kellert on 4/9/18.
//  Copyright © 2018 Nathan Kellert. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    var activityIndicator: UIActivityIndicatorView!
    private var setup = false

    override init(frame: CGRect) {
        super.init(frame: frame)
        activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(activityIndicator)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        if setup { return }
        backgroundColor = UIColor.darkBlueColor.withAlphaComponent(0.7)
        activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        activityIndicator.startAnimating()
        setup = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

