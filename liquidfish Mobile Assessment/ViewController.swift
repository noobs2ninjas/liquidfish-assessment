//
//  ViewController.swift
//  liquidfish Mobile Assessment
//
//  Created by Nathan Kellert on 10/13/19.
//  Copyright © 2019 Nathan Kellert. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var pageText: UITextField!
    
    @IBAction func goPressed(_ sender: UIButton) {
        if let string = pageText.text, !string.isEmpty {
            if let page = Int(string) {
                showImageTable(withPage: page)
                // TODO: Make sure page is greater than 0
            } else {
                // Show error that string is not Int
                showError("Text is not Integer!")
            }
        } else {
            // Send without page(assumes page no is 1)
            showImageTable(withPage: 1)
        }
    }
    
    func showImageTable(withPage page: Int) {
        let imageTableVC = ImageTableViewController()
        imageTableVC.page = page
        
        let navVC = UINavigationController(rootViewController: imageTableVC)
        present(navVC, animated: true, completion: nil)
    }
    
    // TODO: Show error string in UIAlertViewController
    func showError(_ string: String) {
        print(string)
    }
    

}
