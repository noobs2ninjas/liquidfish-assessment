//
//  extensions.swift
//  liquidfish Mobile Assessment
//
//  Created by Nathan Kellert on 10/13/19.
//  Copyright © 2019 Nathan Kellert. All rights reserved.
//

import UIKit
import Bolts

extension UIColor {
    
    static let lightBlueColor = UIColor(red:0.00, green:0.69, blue:0.91, alpha:1.00)
    static let orangeColor = UIColor(red:1.00, green:0.69, blue:0.29, alpha:1.00)
    static let darkBlueColor = UIColor(red:0.00, green:0.43, blue:0.55, alpha:1.00)
}

extension UIImage {
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size

        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height

        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }

        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
}

public enum AsyncDownloadError: Error {
    case noDataRecieved
    case fileNotImage
    case nothingDone
    case noDelegate

    var localizedDescription: String {
        switch self {
        case .noDataRecieved: return "Data not recieved"
        case .fileNotImage: return "File recieved not image"
        case .nothingDone: return "Default Task Returned"
        case .noDelegate: return "No Delegate"
        }
    }
}

